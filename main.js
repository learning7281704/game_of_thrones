$.ajax({
  method: "GET",
  url: "https://thronesapi.com/api/v2/Characters",
}).done((e) => {
  //   console.log(e);
  build_cards(e);
});

const build_cards = (data) => {
  let character_set = "";
  for (let i = 0; i < data.length; ++i) {
    let titles = data[i]["title"].split(",");
    character_set += `
        <div class="card" style="width: 18rem;" onclick="show_character(${data[i]["id"]})">
            <img class="card-img-top" src="${data[i]["imageUrl"]}" alt="${data[i]["fullName"]} image">
            <div class="card-body">
                <h5 class="card-title">${data[i]["fullName"]}</h5>
                <p class="card-text">
                    <div class="badge badge-pill bg-primary">${titles[0]}</div>
                </p>
            </div>
        </div>
        `;
  }
  $("#container").html(character_set);
};

const show_character = (char_id) => {
  $.ajax({
    method: "GET",
    url: `https://thronesapi.com/api/v2/Characters/${char_id}`,
  }).done((e) => {
    console.log(e);
    $("#modal-char-name").html(e["fullName"]);
    $("#modal-char-fullname").html(e["fullName"]);
    $("#modal-char-image").attr("src", e["imageUrl"]);
    $("#modal-char-family").html(e["family"]);
    $("#modal-char-title").html(e["title"]);
    $("#modal").modal("show");
  });
};

$("#search-inp").on("input", (e) => {
  let text = $("#search-inp").val().toLowerCase();

  let chars = $(".card");

  for (let i = 0; i < chars.length; ++i) {
    let temp = $(".card").get(i);
    temp.style.display = "flex";
  }

  for (let i = 0; i < chars.length; ++i) {
    let name = $(".card-title").get(i);
    name = name.innerText.toLowerCase();

    if (name.includes(text) == false) {
      let temp = $(".card").get(i);
      temp.style.display = "none";
    }
  }
});
